#include <stdio.h>
#include "alocador.h"

int main (){
	void *p, *q, *r;
	iniciaAlocador();
	p = alocaMem(100);
	imprimeMapa();
	printf("\n");
	q = alocaMem(50);
	imprimeMapa();
	printf("\n");
	r = alocaMem(500);
	imprimeMapa();
	printf("\n");
	liberaMem(q);
	imprimeMapa();
	printf("\n");
	liberaMem(p);
	imprimeMapa();
	printf("\n");
	q = alocaMem(1);
	p = alocaMem(25);
	finalizaAlocador();
	return 0;
}

