.section .data
	topoInicialHeap:    .quad 0
	topoHeap:           .quad 0
	topoHeapOcupada:    .quad 0
	inicioListaLivre:   .quad 0
	inicioListaOcupada: .quad 0
	fimListaLivre: 		.quad 0
	fimListaOcupada:    .quad 0
	str: 			    .string "'%p',\n"
	strHash: 			.string "\#"
	strMinus:			.string "-"
	strPlus: 			.string "+"
	strNewLine:			.string "\n"
.section .text
.globl iniciaAlocador
.globl finalizaAlocador
.globl alocaMem
.globl liberaMem
.globl imprimeMapa
.type iniciaAlocador, @function
.type finalizaAlocador, @function
.type alocaMem, @function
.type liberaMem, @function
.type imprimeMapa, @function
iniciaAlocador:
	pushq %rbp
	movq %rsp, %rbp
	subq $16, %rsp				#abre espaco pra guardar o que tiver no rax e no rdi
	movq %rax, -8(%rbp)
	movq %rdi, -16(%rbp)
	movq $0, %rdi        		#se %rdi=0, recebemos valor atual de brk
	movq $12, %rax		 		#codigo do syscall brk
	syscall   			 		#chama syscall (recebe valor do brk em %rax)
	movq %rax, topoInicialHeap	#move o valor pra variavel global
	movq %rax, topoHeap 		
	movq %rax, topoHeapOcupada
	movq -8(%rbp), %rax			#retorna ao rax e ao rdi o que lhes pertence
	movq -16(%rbp), %rdi
	addq $16, %rsp
	popq %rbp
	ret
finalizaAlocador:
	pushq %rbp
	movq %rsp, %rbp
	subq $16, %rsp
	movq %rax, -8(%rbp) 		#guarda o %rax e o %rdi atual na pilha
	movq %rdi, -16(%rbp) 
	movq topoInicialHeap, %rdi  #se %rdi != 0, brk <- %rdi
	movq $12, %rax				#syscall pra brk
	syscall 					#restora o valor inicial da heap
	movq -8(%rbp), %rax
	movq -16(%rbp), %rdi 		#restaura o valor de %rdi
	movq $0, topoInicialHeap
	addq $16, %rsp
	popq %rbp
	ret
liberaMem:
	pushq %rbp
	movq %rsp, %rbp
	movq %rdi, %r9
	subq $40, %r9				#pega end inicial do nodo
	movq $0, -40(%rdi)			#bota 0 no campo de livre/ocupado para indicar que está livre
	movq 32(%r9), %r10
	movq %r10, 8(%r9)			#atualiza com tamanho real
	movq inicioListaLivre, %r10
	movq fimListaLivre, %r11
	movq 16(%r9), %rax			#atualiza ponteiros da lista ocupada
	movq 24(%r9), %rcx
	movq %rcx, 24(%rax)
	movq %rax, 16(%rcx)
	movq fimListaOcupada, %r8
	cmpq inicioListaOcupada, %r8
	jne testeInicioOcupado
	movq $0, inicioListaOcupada
	movq $0, fimListaOcupada
	jmp checaInicializacaoListaLivre
testeInicioOcupado:
	cmpq %r8, %r9				#se for o nodo final
	jne elseInicioOcupado
	movq %rax, fimListaOcupada
elseInicioOcupado:
	movq inicioListaOcupada, %r8
	cmpq %r8, %r9				#se for o nodo inicial
	jne checaInicializacaoListaLivre
	movq %rcx, inicioListaOcupada
checaInicializacaoListaLivre:
	cmpq $0, %r10
	jne atualizaListaLivre		#inicializa a lista
	movq %r9, inicioListaLivre	
	movq %r9, fimListaLivre		
	movq %r9, 16(%r9)			#nodoLivre->prev = nodoLivre
	movq %r9, 24(%r9)			#nodoLivre->next = nodoLivre
	jmp merge
atualizaListaLivre:
	movq %r11, 16(%r9)			#atualiza os ponteiros da listaLivre
	movq %r10, 24(%r9)			
	movq %r9, 24(%r11)
	movq %r9, 16(%r10)
	movq %r9, inicioListaLivre
merge:
	movq %r9, %rdx	
	movq 24(%rdx), %rdx
loopAnteriorMem:
	movq %rdx, %rcx
	addq 8(%rdx), %rdx
	addq $40, %rdx
	cmpq %rdx, %r9			#compara pra ver se achou o nodo anterior a %r9
	je mergeAnterior
	movq 24(%rcx), %rdx
	cmpq %rdx, %r9
	jne loopAnteriorMem
	jmp mergeProximo
mergeAnterior:
	movq 8(%rcx), %rsi
	addq 8(%r9), %rsi		#soma os tamanhos
	addq $40, %rsi			#recupera espaco que era das infos gerenciais
	movq %rsi, 8(%rcx)
	movq 24(%r9), %rsi		#altera os enderecos dos ponteiros
	movq %rcx, 16(%rsi)
	movq %rsi, 24(%rcx)
	movq fimListaLivre, %r11
	cmpq %r11, %r9				#se for o nodo final da lista livre
	jne elseILMergeAnt
	movq 16(%r9), %r8
	movq %r8, fimListaLivre
elseILMergeAnt:
	movq inicioListaLivre, %r11
	cmpq %r11, %r9				#se for o nodo inicial da lista livre
	jne atualizaR9
	movq 24(%r9), %r8
	movq %r8, inicioListaLivre
atualizaR9:
	movq %rcx, %r9	
mergeProximo:
	movq %r9, %rdx	
	addq 8(%rdx), %rdx
	addq $40, %rdx
	cmpq topoHeapOcupada, %rdx
	jge fimLiberaMem
	movq 0(%rdx), %rcx
	cmpq $0, %rcx			#se o proximo nodo na memoria for livre, da merge entre eles
	jne fimLiberaMem
	movq 8(%r9), %rsi
	addq 8(%rdx), %rsi
	addq $40, %rsi			#soma tamanhos
	movq %rsi, 8(%r9)		#novo tamanho
	movq 24(%rdx), %rsi
	movq %r9, 16(%rsi)
	movq %rsi, 24(%r9)
	movq fimListaLivre, %r11
	cmpq %r11, %rdx			#se for o nodo final da lista livre
	jne elseILMergeProx
	movq 16(%rdx), %r8
	movq %r8, fimListaLivre
elseILMergeProx:
	movq inicioListaLivre, %r11
	cmpq %r11, %rdx			#se for o nodo inicial da lista livre
	jne fimLiberaMem
	movq 24(%rdx), %r8
	movq %r8, inicioListaLivre
fimLiberaMem:
	movq 8(%r9), %r11
	movq %r11, 32(%r9)
	popq %rbp
	ret
alocaMem:
	pushq %rbp
	movq %rsp, %rbp
	subq $56, %rsp
	movq %rbx, -8(%rbp)
	movq %r12, -16(%rbp)
	movq %r13, -24(%rbp)
	movq %r14, -32(%rbp)
	movq %r15, -40(%rbp)
#comparacao inicial
	movq topoInicialHeap, %r13
	movq topoHeapOcupada, %r14
	movq topoHeap, %r15
	cmpq %r13, %r15				#se ainda não foi alocado espaço nenhum
	je alocaEspacoNovo			#aloca novo espaço para nos
#endcomparacao
#busca
	movq inicioListaLivre, %rbx	#pega o inicio da lista pra busca
	cmpq $0, %rbx
	je checaEspaco
	movq %rbx, %r10
	movq $0, %r11				#menor = 0
loopBusca:
	movq 8(%r10), %r12			#pega o valor do tamanho
	cmpq %rdi, %r12
	jl continueBusca			#se o nodo for de tamanho menor que o necessario
	cmpq $0, %r11							
	jne comparaMenor			#para o primeiro caso, se menor = 0, menor = %r12
	movq %r12, %r11
	movq %r10, %r14				#atualiza endereço pra alocar nodo
comparaMenor:
	cmpq %r11, %r12
	jge continueBusca			#se %r12 < menor, menor = %r12
	movq %r12, %r11
	movq %r10, %r14				#atualiza o endereço pra alocar o nodo
continueBusca:
	movq 24(%r10), %r10
	cmpq %rbx, %r10
	jne loopBusca
#endbusca
checaTipoAlocacao:
	cmpq $0, %r11
	jne atualizaLista			#se menor = 0, nao ha nodos livres
checaEspaco:
	movq %rdi, %r12 			#pega o tamanho
	addq $40, %r12				# + 40
	addq %r14, %r12				# + topoHeapOcupado
	cmpq %r12, %r15				
	jge alocaNovoNo				#topoHeap < topoHeapOcupado + 40 + tam, aloca mais espaço
alocaEspacoNovo:
	movq %rdi, %r12
	addq $40, %r12				#pega o valor total do espaço que ele precisa
alocaEspacoLoop:
	movq %rax, -48(%rbp)		#salva rax
	movq %rdi, -56(%rbp) 		#salva o rdi atual de novo, que nesse caso eh o tamanho a ser alocado
	movq $4096, %rdi			#valor de 4k
	addq %r15, %rdi				#topoHeap + 4k
	movq $12, %rax
	syscall						#altera o valor da brk add 4k
	movq %rdi, topoHeap			#atualiza o valor do topo da heap
	movq topoHeap, %r15	#TIRAR ISSO (eh so pra testar no gdb)
	movq -48(%rbp), %rax 		#restora o valor de %rax
	movq -56(%rbp), %rdi		#restora o valor de %rdi
	subq $4096, %r12			#alocou 4096, tira do necessario
	cmpq $0, %r12				#se o espaço necessario > 0 ainda
	jg alocaEspacoLoop
#endalocaEspacoNovo
alocaNovoNo:
	movq %r14, %r12
	addq $40, %r12
	addq %rdi, %r12
	movq %r12, topoHeapOcupada	#atualiza o valor do topoHeapOcupado
	movq %rdi, 32(%r14)
	movq inicioListaOcupada, %rbx
	jmp checaInicializacaoListaOcupada
#endAlocaNovoNo	
atualizaLista:	
	movq fimListaOcupada, %rax
	movq inicioListaOcupada, %rbx
	movq 16(%r14), %r10			#atualiza ponteiros da lista ocupada
	movq 24(%r14), %r11
	movq %r11, 24(%r10)
	movq %r10, 16(%r11)
	movq fimListaLivre, %r12	
	cmpq inicioListaLivre, %r12
	jne testeInicioLivre
	movq $0, inicioListaLivre
	movq $0, fimListaLivre
	jmp checaInicializacaoListaOcupada
testeInicioLivre:
	cmpq %r12, %r14				#se for o nodo final da lista livre
	jne elseInicioLivre
	movq %r10, fimListaLivre
elseInicioLivre:
	movq inicioListaLivre, %r12
	cmpq %r12, %r14				#se for o nodo inicial da lista livre
	jne checaInicializacaoListaOcupada
	movq %r11, inicioListaLivre
checaInicializacaoListaOcupada:
	cmpq $0, %rbx
	jne atualizaListaOcupada	#se a lista estiver iniciando
	movq %r14, inicioListaOcupada
	movq %r14, fimListaOcupada
	movq %r14, 16(%r14)			#nodo->prev = nodo
	movq %r14, 24(%r14)			#nodo->next = nodo
	jmp alocaNo
atualizaListaOcupada:
	movq fimListaOcupada, %rax
	movq inicioListaOcupada, %rbx
	movq %rax, 16(%r14)			#atualiza os ponteiros da lista
	movq %rbx, 24(%r14)			
	movq %r14, 24(%rax)
	movq %r14, 16(%rbx)
	movq %r14, inicioListaOcupada
alocaNo:
	movq $1, 0(%r14)			#guarda o valor de ocupado
	movq %rdi, 8(%r14) 			#guarda o tamanho do no
	addq $40, %r14 				#pega o endereco do espaco alocado
	movq %r14, %rax				#valor de retorno 			
#endAlocaNo
	movq -40(%rbp), %r15
	movq -32(%rbp), %r14
	movq -24(%rbp), %r13
	movq -16(%rbp), %r12
	movq -8(%rbp), %rbx
	addq $56, %rsp
	popq %rbp
	ret
imprimeMapa:
	pushq %rbp
	movq %rsp, %rbp
	subq $40, %rsp
	movq %rbx, -8(%rbp)
	movq %r12, -16(%rbp)
	movq %r13, -24(%rbp)
	movq %r14, -32(%rbp)
	movq %r15, -40(%rbp)
	movq topoHeapOcupada, %r12
	movq topoInicialHeap, %rbx
loopImprime:
	cmpq %rbx, %r12				
	jle endImprime				#enquanto topoHeapOcupada > nodoAtual, continua
	movq $40, %r14
	movq $0, %r15
forHash:
	cmpq %r14, %r15
	jge fimForHash
	movq $0, %rax
	movq $strHash, %rdi
	call printf
	addq $1, %r15
	jmp forHash
fimForHash:	
	movq 0(%rbx), %r13			#pega o valor de ocupado
	cmpq $1, %r13
	jne elseNaoOcupado
	movq $strPlus, %r13
	jmp fimIfOcupado
elseNaoOcupado:
	movq $strMinus, %r13
fimIfOcupado:
	movq 32(%rbx), %r14			#pega o tamanho
	movq $0, %r15
forBytesEndereco:
	cmpq %r14, %r15
	jge fimForBytes
	movq $0, %rax
	movq %r13, %rdi
	call printf
	addq $1, %r15
	jmp forBytesEndereco
fimForBytes:
	addq %r14, %rbx				#nodo += nodo->tam
	addq $40, %rbx				#nodo += 40;
	jmp loopImprime
	#endimprime
endImprime:
	movq -40(%rbp), %r15
	movq -32(%rbp), %r14
	movq -24(%rbp), %r13
	movq -16(%rbp), %r12
	movq -8(%rbp), %rbx
	addq $40, %rsp
	popq %rbp
	ret
