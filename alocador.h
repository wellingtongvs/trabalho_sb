#ifndef __ALOCADOR__
#define __ALOCADOR__

void iniciaAlocador();			//inicializa pegando o valor da BRK
void finalizaAlocador();		//restaura o topo original da BRK
void* alocaMem(int num_bytes);	//aloca memória a partir de uma certa quantia de bytes
int liberaMem(void* bloco);		//libera um bloco alocado
void imprimeMapa();				//imprime um mapa da heap

#endif

