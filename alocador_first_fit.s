.section .data
	topoInicialHeap: .quad 0
	topoHeap:        .quad 0
	topoHeapOcupada: .quad 0
	str: 			 .string "'%p',\n"
.section .text
.globl main
iniciaAlocador:
	pushq %rbp
	movq %rsp, %rbp
	subq $16, %rsp				#abre espaco pra guardar o que tiver no rax e no rdi
	movq %rax, -8(%rbp)
	movq %rdi, -16(%rbp)
	movq $0, %rdi        		#se %rdi=0, recebemos valor atual de brk
	movq $12, %rax		 		#codigo do syscall brk
	syscall   			 		#chama syscall (recebe valor do brk em %rax)
	movq %rax, topoInicialHeap	#move o valor pra variavel global
	movq %rax, topoHeap 		
	movq %rax, topoHeapOcupada
	movq -8(%rbp), %rax			#retorna ao rax e ao rdi o que lhes pertence
	movq -16(%rbp), %rdi
	addq $16, %rsp
	popq %rbp
	ret
finalizaAlocador:
	pushq %rbp
	movq %rsp, %rbp
	subq $16, %rsp
	movq %rax, -8(%rbp) 		#guarda o %rax e o %rdi atual na pilha
	movq %rdi, -16(%rbp) 
	movq topoInicialHeap, %rdi  #se %rdi != 0, brk <- %rdi
	movq $12, %rax				#syscall pra brk
	syscall 					#restora o valor inicial da heap
	movq -8(%rbp), %rax
	movq -16(%rbp), %rdi 		#restaura o valor de %rdi
	movq $0, topoInicialHeap
	addq $16, %rsp
	popq %rbp
	ret
liberaMem:
	pushq %rbp
	movq %rsp, %rbp
	movq $0, -16(%rdi)			#bota 0 no campo de livre/ocupado para indicar que está livre
	popq %rbp
	ret
alocaMem:
	pushq %rbp
	movq %rsp, %rbp
	subq $56, %rsp
	movq %rbx, -8(%rbp)
	movq %r12, -16(%rbp)
	movq %r13, -24(%rbp)
	movq %r14, -32(%rbp)
	movq %r15, -40(%rbp)
	#comparacao inicial
	movq topoInicialHeap, %r13
	movq topoHeapOcupada, %r14
	movq topoHeap, %r15
	cmpq %r13, %r15				#se ainda não foi alocado espaço nenhum
	je alocaEspacoNovo			#aloca novo espaço para nos
	#endcomparacao
	#busca
	movq %r13, %rbx
loopBusca:
	cmpq %rbx, %r14				
	jle checaEspaco				#enquanto topoHeapOcupada > nodoAtual, continua busca
	movq 0(%rbx), %r12			#pega o valor de ocupado
	cmpq $0, %r12				
	jne continueBusca			#se o nodo estiver ocupado
	movq 8(%rbx), %r12			#pega o valor do tamanho
	cmpq %rdi, %r12
	jl continueBusca			#se o nodo for de tamanho menor que o necessario
	movq %rbx, %r14				#atualiza o endereço pra alocar o nodo
	jmp alocaNo
continueBusca:
	movq 8(%rbx), %r12
	addq %r12, %rbx				#nodo += nodo->tam
	addq $16, %rbx				#nodo += 16;
	jmp loopBusca
	#endbusca
checaEspaco:
	movq %rdi, %r12 			#pega o tamanho
	addq $16, %r12				# + 16
	addq %r14, %r12				# + topoHeapOcupado
	cmpq %r12, %r15				
	jge alocaNovoNo				#topoHeap < topoHeapOcupado + 16 + tam, aloca mais espaço
alocaEspacoNovo:
	#alocaEspacoNovo
	movq %rdi, %r12
	addq $16, %r12				#pega o valor total do espaço que ele precisa
alocaEspacoLoop:
	movq %rax, -48(%rbp)			#salva rax
	movq %rdi, -56(%rbp) 		#salva o rdi atual de novo, que nesse caso eh o tamanho a ser alocado
	movq $4096, %rdi			#valor de 4k
	addq %r15, %rdi				#topoHeap + 4k
	movq $12, %rax
	syscall						#altera o valor da brk add 4k
	movq %rdi, topoHeap			#atualiza o valor do topo da heap
	movq topoHeap, %r15	#TIRAR ISSO
	movq -48(%rbp), %rax 		#restora o valor de %rax
	movq -56(%rbp), %rdi		#restora o valor de %rdi
	subq $4096, %r12			#alocou 4096, tira do necessario
	cmpq $0, %r12				#se o espaço necessario > 0 ainda
	jg alocaEspacoLoop
	#endalocaEspacoNovo
alocaNovoNo:
	#alocaNovoNo
	movq %r14, %r12
	addq $16, %r12
	addq %rdi, %r12
	movq %r12, topoHeapOcupada	#atualiza o valor do topoHeapOcupado
	#endAlocaNovoNo
alocaNo:
	#alocaNo
	movq $1, 0(%r14)			#guarda o valor de ocupado
	movq %rdi, 8(%r14) 			#guarda o tamanho do no
	addq $16, %r14 				#pega o endereco do espaco alocado
	movq %r14, %rax				#valor de retorno 			
	#endAlocaNo
	movq -40(%rbp), %r15
	movq -32(%rbp), %r14
	movq -24(%rbp), %r13
	movq -16(%rbp), %r12
	movq -8(%rbp), %rbx
	addq $56, %rsp
	popq %rbp
	ret
main: 
	pushq %rbp
	movq %rsp, %rbp
	subq $16, %rsp
	movq $0, %rax
	movq %rdi, -8(%rbp)
	movq %rsi, -16(%rbp)
	call iniciaAlocador
	movq $100, %rdi
	call alocaMem
	movq $str, %rdi
	movq %rax, %rsi
	call printf
	movq $5000, %rdi
	call alocaMem
	movq %rax, %r12
	movq $str, %rdi
	movq %rax, %rsi
	call printf
	movq $200, %rdi
	call alocaMem
	movq $str, %rdi
	movq %rax, %rsi
	call printf
	movq %r12, %rdi
	call liberaMem
	movq $4000, %rdi
	call alocaMem
	movq $str, %rdi
	movq %rax, %rsi
	call printf
	movq $100, %rdi
	call alocaMem
	movq $str, %rdi
	movq %rax, %rsi
	call printf
	movq -8(%rbp), %rdi
	movq -16(%rbp), %rsi
	addq $16, %rsp
	movq $60, %rax
	syscall
