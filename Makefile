all: wlmalloc

wlmalloc: wlmalloc.o alocador.o
		gcc -c wlmalloc.c
		gcc -c alocador.s
		gcc -static wlmalloc.o alocador.o -o wlmalloc
clean:
	-rm wlmalloc.o alocador.o
purge: clean
	-rm wlmalloc
